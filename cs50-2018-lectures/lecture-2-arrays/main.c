// CS50 2018 - Lecture 2 - Arrays
// Mar 4, 2023

/*
- preprocessing
- compiling
    assembly instructions
- assembling
    00001010110011101
- linking
    hello.c  cs50.c  printf.c

*/

#include <stdio.h> // pre-processor directives

int main()
{

    int array[3][2] = {
        {2, 3},
        {4, 5},
        {6, 7}};

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 2; ++j)
        {
            printf("%d ", array[i][j]);
        }
        printf("\n");
    }

    return 0;
}