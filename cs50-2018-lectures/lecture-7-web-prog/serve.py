from http.server import BaseHTTPRequestHandler, HTTPServer

class HTTPServer_RequestHandler(BaseHTTPRequestHandler):
    
    def do_GET(self):
        self.send_response(200)

        self.send_header("Content-type", "text/html")
        self.end_headers()

        self.wfile.write(b"<!DOCTYPE html>")
        self.wfile.write(b"<html lang='en'>")
        self.wfile.write(b"<head><title>FLASK</title></head>")
        self.wfile.write(b"<body>hello</body></html>")

port = 8080
server_address = ("localhost", port)
httpd = HTTPServer(server_address, HTTPServer_RequestHandler)

httpd.serve_forever()