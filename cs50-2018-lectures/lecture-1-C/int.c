#include <stdio.h>

int main()
{
    double x;
    double y;

    printf("enter x: ");
    scanf("%lf",&x);

    printf("enter y: ");
    scanf("%lf",&y);

    printf("result: %.50lf\n", x/y);

    // result: 0.20000000298023223876953125000000000000000000000000 (float)
    // result: 0.20000000000000001110223024625156540423631668090820 (double)

    return 0;
}