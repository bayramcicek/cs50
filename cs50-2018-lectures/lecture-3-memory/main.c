#include <stdio.h>
#include <string.h>
#include <stdbool.h>

bool compareStrings(char*, char*);

int main(void)
{
    char s[50];
    char t[50];
    printf("Yout name: ");
    scanf("%s", s);

    if (s == NULL)
    {
        return 1;
    }

    char* newMem = malloc(strlen(s) + 1);

    printf("Yout name: ");
    scanf("%s", t);

    if (compareStrings(s, t))
    {
        printf("same\n");
    }
    else
    {
        printf("different\n");
    }

    return 0;
}

bool compareStrings(char* a, char* b)
{
    if (strlen(a) != strlen(b))
    {
        return false;
    }
    for (int i = 0, n = strlen(a); i < n; ++i)
    {
        if (a[i] != b[i])
        {
            return false;
        }
    }

    return true;
}