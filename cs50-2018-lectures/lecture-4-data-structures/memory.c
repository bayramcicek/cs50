// implements a list of numbers using a linked list

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <memory.h>
#include <stdlib.h>

typedef struct node
{
    int number;
    struct node* next;
} node;

int main(int argc, char** argv)
{
    node* numbers = NULL;

    while (true)
    {
        int number;
        printf("number: ");
        scanf("%d", &number);

        if (number == EOF)
        {
            break;
        }

        node* n = malloc(sizeof(node));
        if (!n)
        {
            return 1;
        }

        // add number to list
        (*n).number = number;
        n->next = NULL;

        if (numbers)
        {
            for (node* ptr = numbers; ptr != NULL; ptr = ptr->next)
            {
                if (!ptr->next)
                {
                    ptr->next = n;
                    break;
                }
            }
        }
        else
        {
            numbers = n;
        }
    }

    for (node* ptr = numbers; ptr != NULL; ptr = ptr->next)
    {
        printf("number : %d\n", ptr->number);
    }

    free(numbers);

    return 0;
}