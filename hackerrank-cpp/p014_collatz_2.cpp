// C++17 standard
// created by cicek on Jan 13, 2022 3:33 PM

// Project Euler #14: Longest Collatz sequence
// https://www.hackerrank.com/contests/projecteuler/challenges/euler014/problem

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <vector>

int main()
{
    int input = 0;
    std::cin >> input;

    std::vector<int> numbers(input);

    for (int i = 0; i < input; i++)
    {
        std::cin >> numbers[i];
    }

    for (int limit : numbers)
    {
        std::vector<size_t> collatz{};

        for (int i = 1; i <= limit; i++)
        {
            std::vector<int> sequence{};
            int temp = i;

            while (temp != 1)
            { // even
                if (temp % 2 == 0)
                {
                    temp /= 2;
                }
                else // odd
                {
                    temp = (3 * temp) + 1;
                }
                sequence.push_back(temp);
            }
            collatz.push_back(sequence.size());
        }
        //        for (size_t i : collatz)
        //        {
        //            std::cout << i << ' ';
        //        }
        std::cout << '\n'
                  << (std::max_element(collatz.begin(), collatz.end()))
                         - collatz.begin() + 1;
    }
    return 0;
}
