// C++17 standard
// created by cicek on Jan 13, 2022 3:33 PM

// Project Euler #14: Longest Collatz sequence
// https://www.hackerrank.com/contests/projecteuler/challenges/euler014/problem

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <vector>

int main()
{
    int input = 0;
    std::cin >> input;

    std::vector<int> numbers(input);

    for (int i = 0; i < input; i++)
    {
        std::cin >> numbers[i];
    }

    for (int limit : numbers)
    {
        int longest_path = 0;
        int longest_chain_num = 0;

        for (int start = 1; start <= limit; start++)
        {
            int path_length = 0;
            int last = start;
            while (true)
            {
                if (last == 1)
                {
                    if (path_length > longest_path)
                    {
                        longest_path = path_length;
                        longest_chain_num = start;

                    }
                    break;
                }

                if (last % 2 == 0) // even
                {
                    last /= 2;
                }
                else
                {
                    last = (3 * last) + 1;
                }
                path_length++;
            }

            start++;
        }

//        std::cout << longest_path << '\n';
        std::cout << longest_chain_num << '\n';
    }

    return 0;
}
