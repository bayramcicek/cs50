// C++17 standard
// created by cicek on Jan 19, 2022 3:33 PM

// Project Euler #14: Longest Collatz sequence
// https://www.hackerrank.com/contests/projecteuler/challenges/euler014/problem

#include <bits/stdc++.h>
#include <vector>

int target = 5000001;
std::vector<long> collatzVector(target, 0);

long countChain(long num)
{
    // collatzVector[1] = 1
    // base case
    if (collatzVector[num] != 0)
        return collatzVector[num];

    if (num % 2 == 0)
        collatzVector[num] = 1 + countChain(num / 2);
    else
    {
        long temp = 0, count = 1;
        temp = num * 3 + 1;
        while (temp > target)
        {
            if (temp % 2 == 0)
                temp /= 2;
            else
                temp = temp * 3 + 1;
            count++;
        }
        collatzVector[num] = count + countChain(temp);
    }
    return collatzVector[num];
}
int main()
{
    long input, item;
    collatzVector[1] = 1;

    for (long a = 1; a <= target; a++)
    {
        countChain(a);
    }

    std::vector<long> results(target);
    results[1] = 1;

    // calculate longest chain
    for (long a = 1; a <= target; a++)
    {
        if (collatzVector[a] >= collatzVector[results[a - 1]])
            results[a] = a;
        else
            results[a] = results[a - 1];
//        std::cout << results[a] << ' ';
    }

    std::cin >> input;
    for (int i = 0; i < input; i++)
    {
        std::cin >> item;
        std::cout << results[item] << '\n';
    }

    return 0;
}