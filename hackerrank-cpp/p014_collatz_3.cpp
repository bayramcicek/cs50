// C++17 standard
// created by cicek on Jan 13, 2022 3:33 PM

// Project Euler #14: Longest Collatz sequence
// https://www.hackerrank.com/contests/projecteuler/challenges/euler014/problem

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <iterator>
#include <map>
#include <vector>

int main()
{
    int input = 0;
    std::cin >> input;

    std::vector<int> numbers(input);
    std::map<long int, long int> collatzMap{};

    for (size_t i = 0; i < input; i++)
    {
        std::cin >> numbers[i];
    }

    for (int limit : numbers)
    {
        for (long int i = 1; i <= limit; i++)
        {
            long int temp = i;
            long int path = 0;

            while (temp != 1)
            {
                // std::map<long int, long int>::iterator itrFind
                auto itrFind = collatzMap.find(temp);
                if (itrFind == collatzMap.end())
                {
                    if (temp % 2 == 0) // even
                    {
                        temp /= 2;
                    }
                    else // odd
                    {
                        temp = (3 * temp) + 1;
                    }
                    path++;
                }
                else
                {
                    path += itrFind->second;
                    break;
                }
            }
            collatzMap.insert(std::pair<long int, long int>(i, path));
        }
        auto x = std::max_element(collatzMap.rbegin(), collatzMap.rend(),
                                  [](const std::pair<long int, long int> &p1,
                                     const std::pair<long int, long int> &p2)
                                  { return p1.second < p2.second; });
        std::cout << x->first << '\n';
    }
    //    std::map<long int, long int>::iterator itr;
    //    for (itr = collatzMap.begin(); itr != collatzMap.end(); itr++)
    //    {
    //        std::cout << itr->first << '-' << itr->second << '\n';
    //    }
    return 0;
}