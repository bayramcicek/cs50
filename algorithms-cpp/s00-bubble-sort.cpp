#include <iostream>
#include <array>

int main()
{
    // std::array arr = { 6, 3, 1, 8, 5, 9, 4, 2, 0, 7 }; // since C++17
    std::array<int, 10> arr = { 6, 3, 1, 8, 5, 9, 4, 2, 0, 7 }; // before C++17

    for (int i = arr.size() - 1; i > 0; --i)
    {
        bool isSwapped = false;
        for (int j = 0; j < i; ++j)
        {
            if (arr[j] > arr[j + 1])
            {
                isSwapped = true;
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
        if (!isSwapped)
            break;

        // for (auto& i : arr)
        // {
        //     std::cout << i << ' ';
        // }
        // std::cout << '\n';
    }

    for (auto& i : arr)
    {
        std::cout << i << ' ';
    }
    std::cout << '\n';

    return 0;
}

/*
Time Complexity:
- O(n^2) Worst case performance
- O(n) Best-case performance
- O(n^2) Average performance

Space Complexity:
- O(1) Worst case
*/