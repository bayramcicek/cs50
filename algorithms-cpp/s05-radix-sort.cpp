#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

void radixsort(int arr[], int length)
{
    int count[10];
    int* output = new int[length];

    memset(output, 0, length * sizeof(*output));
    memset(count, 0, sizeof(count));

    int max = 0;
    for (int i = 0; i < length; ++i)
    {
        if (arr[i] > max)
        {
            max = arr[i];
        }
    }

    int maxdigits = 0;
    while (max)
    {
        maxdigits++;
        max /= 10;
    }
    for (int j = 0; j < maxdigits; j++)
    {
        for (int i = 0; i < length; i++)
        {
            int t = std::pow(10, j);
            count[(arr[i] / (10 * t)) % 10]++;
        }

        int k = 0;
        for (int p = 0; p < 10; p++)
        {
            for (int i = 0; i < length; i++)
            {
                int t = std::pow(10, j);
                if ((arr[i] % (10 * t)) / t == p)
                {
                    output[k] = arr[i];
                    k++;
                }
            }
        }
        
        memset(count, 0, sizeof(count));
        for (int i = 0; i < length; ++i)
        {
            arr[i] = output[i];
        }
    }
    delete[] output;
}

void print(int arr[], int length)
{
    for (int i = 0; i < length; ++i)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << '\n';
}

int main(int argc, char const* argv[])
{
    int array[] = { 170, 1453, 45, 75, 90, 802, 24, 2, 66 };
    int length = sizeof(array) / sizeof(array[0]);
    radixsort(array, length);
    print(array, length);
    return 0;
}

/*

Time Complexity	 
Best	O(nk)
Worst	O(nk)
Average	O(nk)

n: length of array
k: number of digits (average)

Space Complexity
O(n+k)

*/
