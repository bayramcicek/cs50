#include <iostream>
#include <array>

template <size_t T> void printArray(std::array<int, T>& arr)
{
    for (int i = 0; i < arr.size(); ++i)
    {
        std::cout << arr[i] << ' ';
    }
}

template <size_t T> void insertionSort(std::array<int, T>& arr)
{
    for (int i = 1; i < arr.size(); ++i)
    {
        int temp = arr[i];
        int j = i - 1;
        while (j >= 0 && temp < arr[j])
        {
            arr[j + 1] = arr[j];
            --j;
        }
        arr[j + 1] = temp;
    }
}

int main()
{
    std::array<int, 13> arr = { 11, 3, 10, 8, 5, 12, 4, 2, 9, 0, 7, 6, 1 }; // before C++17

    printArray(arr);
    std::cout << '\n';

    insertionSort(arr);

    printArray(arr);
    std::cout << '\n';

    return 0;
}

/*
Time Complexity
- О(n^2) comparisons, О(n^2) swaps -- Worst Case

- O(n) comparisons, O(1) swaps -- Best Case

- O(n^2) Average performance

Space Complexity
O(1) -- (No extra space needed, sorting done in place)
*/