#include <iostream>
#include <time.h>

void merge(int* arr, int l, int m, int r)
{
    // create L -> L[l..m] and R -> R[m+1, r]
    int sizeLeft = m - l + 1;
    int sizeRight = r - m;

    int* L = new int[sizeLeft];
    int* R = new int[sizeRight];

    for (int i = 0; i < sizeLeft; ++i)
    {
        L[i] = arr[l + i];
    }

    for (int j = 0; j < sizeRight; ++j)
    {
        R[j] = arr[m + 1 + j];
    }

    // Maintain current index of sub-arrays and main array
    int i, j, k;
    i = 0;
    j = 0;
    k = l;

    // Until we reach either end of either L or R, pick larger among
    // elements L and R and place them in the correct position at A[l..r]
    while (i < sizeLeft && j < sizeRight)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            ++i;
        }
        else
        {
            arr[k] = R[j];
            ++j;
        }
        ++k;
    }

    // When we run out of elements in either L or M,
    // pick up the remaining elements and put in arr[p..r]
    while (i < sizeLeft)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < sizeRight)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int* arr, int l, int r)
{
    if (l < r)
    {
        int m = (l + r) / 2;
        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);
        merge(arr, l, m, r);
    }
}

void printArray(int* arr, int size)
{
    for (int i = 0; i < size; ++i)
    {
        std::cout << arr[i] << ' ';
    }
    std::cout << '\n';
}

int main()
{
    int size = 10;
    int* arr = new int[size];
    std::srand(time(NULL));

    for (int i = 0; i < size; ++i)
    {
        arr[i] = (rand() % 100);
    }

    std::cout << "Random array : ";
    printArray(arr, size);

    mergeSort(arr, 0, size - 1);

    std::cout << "Sorted array : ";
    printArray(arr, size);

    delete[] arr;

    return 0;
}

// https://www.programiz.com/dsa/merge-sort
// https://the-algorithms.com/algorithm/merge-sort?lang=c-plus-plus

/*
Time Complexity
Best case - O(n log n)
Average - O(n log n)
Worst case - O(n log n)

Space Complexity
O(n)
*/