#include <iostream>

int getMax(int* array, int size)
{
    int max = array[0];
    for (int i = 1; i < size; ++i)
    {
        if (array[i] > max)
        {
            max = array[i];
        }
    }
    return max;
}

void printArray(int* arr, int size)
{
    for (int i = 0; i < size; ++i)
    {
        std::cout << arr[i] << ' ';
    }
    std::cout << '\n';
}

void countingSort(int* array, int size, int place)
{
    const int max = 10;
    int output[size];
    int count[max];

    for (int i = 0; i < max; ++i)
        count[i] = 0;

    // Calculate count of elements
    for (int i = 0; i < size; i++)
        count[(array[i] / place) % 10]++;

    // Calculate cumulative count
    for (int i = 1; i < max; i++)
        count[i] += count[i - 1];

    // Place the elements in sorted order
    for (int i = size - 1; i >= 0; i--)
    {
        output[count[(array[i] / place) % 10] - 1] = array[i];
        count[(array[i] / place) % 10]--;
    }

    for (int i = 0; i < size; i++)
        array[i] = output[i];
}

void radixSort(int* arr, int size)
{
    // get max
    int max = getMax(arr, size);

    for (int place = 1; max / place > 0; place *= 10)
    {
        countingSort(arr, size, place);
    }
}

int main()
{
    int array[] = { 121, 432, 564, 23, 1, 45, 788 };
    int size = sizeof(array) / sizeof(array[0]);
    radixSort(array, size);
    printArray(array, size);

    return 0;
}