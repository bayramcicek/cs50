#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

void swap(int* first, int* second)
{
    int temp = *first;
    *first = *second;
    *second = temp;
}

void selectionSort(int* arr, int size)
{
    for (int i = 0; i < size - 1; ++i)
    {
        int min_index = i;
        for (int j = i + 1; j < size; ++j)
        {
            if (arr[j] < arr[min_index])
            {
                min_index = j;
            }
        }
        if (min_index != i)
        {
            swap(arr + i, arr + min_index);
        }
    }
}

int main()
{
    int size = 10;
    int* arr = new int[size];
    std::srand(time(NULL));

    for (int i = 0; i < size; i++)
    {
        arr[i] = (rand() % 50);
    }

    for (int i = 0; i < size; i++)
    {
        std::cout << arr[i] << ' ';
    }
    std::cout << '\n';

    selectionSort(arr, size);

    for (int i = 0; i < size; i++)
    {
        std::cout << arr[i] << ' ';
    }
    std::cout << '\n';
    
    return 0;
}

/* my solution
template <size_t T> void printArray(std::array<int, T>& arr)
{
    for (int i = 0; i < arr.size(); ++i)
    {
        std::cout << arr[i] << ' ';
    }
}

int main()
{
    std::array<int, 13> arr = { 11, 3, 10, 8, 5, 12, 4, 2, 9, 0, 7, 6, 1 }; // before C++17

    // print original array
    printArray(arr);
    std::cout << '\n';

    for (int i = 0; i < arr.size(); ++i)
    {
        int min = arr[i];
        int swapLocation = i;
        for (int j = i; j < arr.size(); ++j)
        {
            if (arr[j] < min)
            {
                min = arr[j];
                swapLocation = j;
            }
        }
        int temp = arr[i];
        arr[i] = min;
        arr[swapLocation] = temp;

        printArray(arr);
        std::cout << '\n';
    }
    std::cout << '\n';

    // print array
    // printArray(arr);
    // std::cout << '\n';

    return 0;
}
*/
/*
Time Complexity
O(n^2) Worst case performance

O(n^2) Best-case performance

O(n^2) Average performance

Space Complexity
O(1) Worst case
*/