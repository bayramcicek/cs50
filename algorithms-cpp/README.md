## Sorting Algorithms in C++
<pre>
+ Bubble sort
+ Insertion sort
+ Selection sort
+ Merge sort
+ Radix sort
+ Quick sort
+ Heap sort
</pre>

---
* Bubble sort:

![Bubble sort](./gif/bubble-sort-example-300px.gif "bubble-sort-gif")

---
* Insertion sort:

![Insertion sort](./gif/insertion-sort-example-300px.gif "Insertion sort")

---
* Selection sort:

![Selection sort](./gif/selection-sort.gif "Selection sort")

---
* Merge sort:

![Merge sort](./gif/merge-sort.gif "Merge sort")

---
* Radix sort:

![Radix sort](./gif/radix-sort.png "Radix sort")

---
* Quick sort:

![Quick sort](./gif/quick-sort.png "Quick sort")

---
* Heap sort:

![Max heap](./gif/max-heap.png "Max heap")

---

![Heap sort](./gif/heap-sort.png "Heap sort")

---

(image credits: wikipedia and various websites)

---

Generally, sorting algorithms are classified into two different categories:

**1. Comparison sort:** `Bubble Sort`, `Selection Sort`, `Insertion Sort`, `Merge Sort`, `Quick Sort`, `Heap Sort`

**2. Non-comparison sort:** `Counting Sort`, `Radix Sort`

---

![Sorting Algorithms](./gif/sorting-algorithms-table.jpg "Sorting Algorithms")

image: https://medium.com/@bill.shantang/8-classical-sorting-algorithms-d048eec3fdab
