#include <stdio.h>
#include <stdlib.h>

void display(int arr[], int length)
{
    for (int i = 0; i < length; i++)
    {
        printf("%d ", arr[i]);
    }

    printf("\n");
}

void swap(int* first, int* second)
{
    int temp = *first;
    *first = *second;
    *second = temp;
}

int partition(int arr[], int lower, int upper)
{
    // pointer for smaller element
    int i = lower - 1;

    // select the rightmost element as pivot
    int pivot = arr[upper];

    // if current element is smaller than the pivot
    for (int j = lower; j < upper; ++j)
    {
        if (arr[j] <= pivot)
        {
            // increment the index of smaller element
            ++i;
            swap(&arr[i], &arr[j]);
        }
    }

    // when reached to pivot, swap arr[++i] with upper/pivot
    swap(&arr[i + 1], &arr[upper]);

    return i + 1;
}

/*This is where the sorting of the array takes place
    arr[] --- Array to be sorted
    lower --- Starting index
    upper --- Ending index
*/

void quickSort(int arr[], int lower, int upper)
{
    if (lower < upper)
    {
        // find the pivot element such that
        // elements smaller than pivot are on left of pivot
        // elements greater than pivot are on right of pivot
        int partitionIndex = partition(arr, lower, upper);

        // recursive call on the left of pivot
        quickSort(arr, lower, partitionIndex - 1);

        // recursive call on the right of pivot
        quickSort(arr, partitionIndex + 1, upper);
    }
}

int main()
{
    int arr[] = { 8, 7, 2, 1, 0, 9, 6 };
    int length = sizeof(arr) / sizeof(arr[0]);

    printf("Original array  : ");
    display(arr, length); // { 8, 7, 2, 1, 0, 9, 6 }

    quickSort(arr, 0, length - 1);

    printf("Sorted array    : ");
    display(arr, length); // { 0, 1, 2, 6, 7, 8, 9 }

    return 0;
}

/*
index:
i = -1;
j = 0;
pivot = length - 1;

if (j < pivot)
{
    ++i;
    swap(arr[i], arr[j])
}
else // j >= pivot
{
    ++j;
}

quick sort = moves smaller elements to left of a pivot.
 recursively divide array in 2 partitions.

run-time complexity:
    Best case       : O(n log n)
    Average case    : O(n log n)
    Worst case      : O(n^2)    (if already sorted)

space complexity:
    O(log n) due to recursion

*/
////////////////////////////////////////////////////////////////////
/*

- Approach
    - Make the right-most index value pivot
    - partition the array using pivot value
    - quicksort left partition recursively
    - quicksort right partition recursively

- Time Complexity
    - O(n^2) Worst case performance
    - O(n log n) Best-case performance
    - O(n log n) Average performance

- Space Complexity
    - O(log n) Worst case

- Founder's Name
    - Tony Hoare in 1959

*/