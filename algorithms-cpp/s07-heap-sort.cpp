// https://github.com/TheAlgorithms/C-Plus-Plus/blob/master/sorting/heap_sort.cpp

#include <algorithm>
#include <iostream>

/**
 *
 * Utility function to print the array after
 * sorting.
 *
 * @param arr array to be printed
 * @param sz size of array
 *
 */
template <typename T> void printArray(T* arr, int size)
{
    for (int i = 0; i < size; i++)
        std::cout << arr[i] << "  ";
    std::cout << "\n";
}

/**
 *
 * \addtogroup sorting Sorting Algorithm
 * @{
 *
 * The heapify procedure can be thought of as building a heap from
 * the bottom up by successively sifting downward to establish the
 * heap property.
 *
 * @param arr array to be sorted
 * @param size size of array
 * @param i node position in Binary Tress or element position in
 *          Array to be compared with it's childern
 *
 */
template <typename T> void heapify(T* arr, int size, int i)
{
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < size && arr[l] > arr[largest])
        largest = l;

    if (r < size && arr[r] > arr[largest])
        largest = r;

    if (largest != i)
    {
        std::swap(arr[i], arr[largest]);
        heapify(arr, size, largest);
    }
}

/**
 * Utilizes heapify procedure to sort
 * the array
 *
 * @param arr array to be sorted
 * @param size size of array
 *
 */
template <typename T> void heapSort(T* arr, int size)
{
    for (int i = size - 1; i >= 0; i--)
        heapify(arr, size, i);

    for (int i = size - 1; i >= 0; i--)
    {
        std::swap(arr[0], arr[i]);
        heapify(arr, i, 0);
    }
}

int main()
{
    int arr[] = { -10, 78, -1, -6, 7, 4, 94, 5, 99, 0 };
    int sz = sizeof(arr) / sizeof(arr[0]); // sz - size of array

    printArray(arr, sz); // displaying the array before sorting
    heapSort(arr, sz); // calling heapsort to sort the array
    printArray(arr, sz); // display array after sorting

    double arr2[] = { 4.5, -3.6, 7.6, 0, 12.9 };
    sz = sizeof(arr2) / sizeof(arr2[0]);

    printArray(arr2, sz);
    heapSort(arr2, sz);
    printArray(arr2, sz);

    return 0;
}

/*
sink:   to (cause something or someone to) go down 
        below the surface or to the bottom of a liquid or soft substance

- sink all non-leaf nodes to construct a heap.
- swap the rightmost leaf with the root, and sink. 

*/

/*

Time Complexity
- O(n log n) Worst case performance
- O(n log n) (distinct keys) or O(n) (equal keys) Best-case performance
- O(n log n) Average performance

Space Complexity
- O(1) Worst case auxiliary

*/